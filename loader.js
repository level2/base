var Ember = require('ember');

// Loader is used to expose the progress of a promise.
var Loader = Ember.Object.extend({
    
    // promise is the promise chain the Loader loads (it is required).
    promise: null,
    
    // loading is true when the Loader is currently loading data, otherwise it is false.
    loading: false,
    
    // result is the data that the promise was resolved with, or null if the promise failed or is loading
    result: null,
    
    // error is the error the promise failed with, or null if the promise succeeded or is loading
    error: null,
    
    // options is an hash containing user defined data for use in templates
    options: null,
    
    // done is true if the loader is not loading currently (opposite of loading)
    done: function() {
        return !this.get('loading');
    }.property('loading'),
    
    // init sets up handlers on the promise chain to control the Loader
    init: function() {
        this._super.apply(this, arguments);
        
        var promise = this.get('promise');
        if(promise == null) {
            throw new Error('Loader.promise can not be blank');
        }
        if(typeof promise.then !== 'function') {
            throw new Error('Loader.promise must be a promise object (containing a promise.then() function)');
        }
        
        if(!this.get('options')) {
            this.set('options', {});
        }
        
        var self = this;
        self.set('loading', true);
        promise.then(function(result) {
            // Handle success
            self.set('loading', false);
            self.set('result', result);
            
        }, function(err) {
            // Handle error
            self.set('loading', false);
            self.set('error', err);
            
        });
    },
    
    // setOption sets a property on the options hash and returns the loader instance for chaining
    setOption: function(key, value) {
        this.set('options.' + key, value);
        return this;
    }
    
});

// Loader.load is a convenience method for creating a Loader with a certain promise.
Loader.load = function(promise) {
    return Loader.create({
        promise: promise
    });
};

// Loader.error is a convenience method for creating a Loader in a faild state
Loader.error = function(message) {
    return Loader.create({
        promise: Ember.RSVP.reject({ message: message })
    });
};

module.exports = Loader;