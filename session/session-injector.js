/**
 * Session Injector
 * 
 * This injects an API session onto all Routes, Controllers, and Components as "session:main".
 */
const Ember = require('ember');
const Session = require('./api-session.js');

module.exports = Ember.Application.initializer({
    name: 'session',
    initialize(app) {
        app.register('session:main', Session, { instantiate: false });
        app.inject('route', 'session', 'session:main');
        app.inject('controller', 'session', 'session:main');
        app.inject('component', 'session', 'session:main');
        app.inject('api:main', 'session', 'session:main');
    }
});