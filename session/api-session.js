/**
 * API Session
 * 
 * This script defines the API session and its data.
 */
const Ember = require('ember');

let Session = Ember.Object.extend({

    /** The session's CSRF key */
    csrfKey: null,

    /** The user model */
    user: null,

    /** True if the session exists and is valid */
    valid: Ember.computed('csrfKey', function() {
        return this.get('csrfKey') != null;
    }),

    /** Sets up the session */
    init() {
        this.set('user', {});
        this.refresh();
    },

    /** Reads the session details from the document cookies */
    refresh() {
        var keyMatch = /swirl\-csrf\-key=([^\&]+)/.exec(document.cookie);
        if(keyMatch != null) {
            this.set('csrfKey', keyMatch[1]);
        } else {
            this.set('csrfKey', null);
        }
    }

});
Session[Ember.NAME_KEY] = 'Session';

module.exports = Session.create();
