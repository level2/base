const Loader = require('./loader.js');
const session = require('./session/api-session.js');

/** Sends an HTTP request to a dcubed api, returning a promise for the results. */
exports.request = function(method, url, body) {
    var reqBody = undefined;
    var headers = {};
    if(body != null) {
        reqBody = JSON.stringify(body);
        headers['Content-Type'] = 'application/json';
    }

    // Add CSRF key if available
    var csrfKey = loadCSRFToken();
    if(csrfKey != null) {
        headers['X-Swirl-CSRF-Key'] = csrfKey;
    }

    var deferred = $.Deferred();
    $.ajax({
        method: method,
        url: url,
        data: reqBody,
        headers: headers,
        xhrFields: {
            withCredentials: true
        }

    }).done(function(data, status, xhr) { // Response Handling
        session.refresh();
        deferred.resolve(data);

    }).fail(function(xhr, status, error) { // Error Handling
        session.refresh();
        if(xhr != null && xhr.responseJSON != null && xhr.responseJSON.message != null) {
            deferred.reject(new exports.APIError(xhr.status, xhr.responseJSON.message, xhr.responseJSON.type));

        } else if(error != null) {
            deferred.reject(new Error('' + error));

        } else {
            deferred.reject(new Error('Failed to request ' + url + ': unknown error'));
        }
    });

    return deferred;
}

/** Gets JSON from a dcubed api endpoint. */
exports.get = function(url) {
    return exports.request('GET', url, null);
}

/** Posts JSON to a dcubed api endpoint. */
exports.post = function(url, body) {
    return exports.request('POST', url, body);
}

/** Puts JSON to a dcubed api endpoint. */
exports.put = function(url, body) {
    return exports.request('PUT', url, body);
}

/** Deletes JSON from a dcubed api endpoint. */
exports.delete = function(url, body) {
    return exports.request('DELETE', url, body);
}

/** Patches JSON to a dcubed api endpoint. */
exports.patch = function(url, body) {
    return exports.request('PATCH', url, body);
}

/** Uploads a file (POST) to a dcubed api endpoint. */
exports.uploadFile = function(url, file, headers) {
    var req = new XMLHttpRequest();
    req.open('POST', url, true);

    headers = headers || {};
    for(var header in headers) {
        req.setRequestHeader(header, headers[header]);
    }
    req.setRequestHeader('Content-Type', 'application/octetstream');
    var csrfKey = loadCSRFToken();
    if(csrfKey != null) {
        req.setRequestHeader('X-Swirl-CSRF-Key', csrfKey);
    }
    
    var dfd = $.Deferred();
    var uploadPromise = dfd.promise();
    uploadPromise.onprogress = function(handler) {
        uploadPromise._onprogress = handler;
        return uploadPromise;
    };

    req.onreadystatechange = function() {
        if(req.readyState != XMLHttpRequest.DONE) return;
        
        var res = {};
        try {
            res = JSON.parse(req.responseText);
        } catch(err) { 
            res = undefined;
        }
        if(req.status < 400) {
            dfd.resolve(res);
        } else {
            dfd.reject(new exports.APIError(req.status, (res && !!res.message) ? res.message : req.responseText, res ? res.type : null));
        }
    };

    if(req.upload != null) {
        req.upload.addEventListener('progress', function(event) {
            if(uploadPromise._onprogress != null) {
                uploadPromise._onprogress(event.loaded || 0, event.total || 0);
            }
        }, false);
    } else {
        req.onprogress = function(event) {
            if(uploadPromise._onprogress != null) {
                uploadPromise._onprogress(event.loaded || 0, event.total || 0);
            }
        };
    }

    req.send(file);

    return uploadPromise;
}

/** Thrown when an API error is encountered. */
exports.APIError = function(statusCode, message, type) {
    this.name = 'APIError';
    this.statusCode = statusCode;
    this.message = message;
    this.type = type;
}
exports.APIError.prototype = new Error;

/** Converts the APIError into a string. */
exports.APIError.prototype.toString = function() {
    return this.name + ': HTTP ' + this.statusCode + ': ' + this.message;
}

/** Loads the Swirl CSRF token, if available. */
function loadCSRFToken() {
    var keyMatch = /swirl\-csrf\-key=([^\&;]+)/.exec(document.cookie);
    if(keyMatch == null) return null;
    
    var key = keyMatch[1];
    if(key == null || !key.trim()) {
        return null;
    }
    return key;
}
