/**
 * Password Module
 * 
 * This module provides a Password object that can be used to build password verification forms.
 */
const Ember = require('ember');

module.exports = Ember.Object.extend({

    /** The password originally. */
    one: null,

    /** The password repeated. */
    two: null,

    /** The error if the passwords are invalid. */
    error: Ember.computed('one', 'two', function() {
        let one = this.get('one');
        let two = this.get('two');

        if(!one) return null;
        if(one !== two) return 'Password must match';
        return null;
    }),

    /** The password to use. */
    value: Ember.computed('error', 'two', function() {
        if(!!this.get('error')) {
            return null;
        }
        return this.get('two');
    })

});
module.exports[Ember.NAME_KEY] = 'Password';